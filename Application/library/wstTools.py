import os

from PyQt5.QtCore import QSize

from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QPixmap

from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QSpacerItem
from PyQt5.QtWidgets import QSizePolicy

def wildCardSearchString(target, wildCard):
    if '*' not in wildCard:
        if wildCard in target:
            return True
        else:
            return False
        #end if
    #end if
    wildParts = wildCard.split('*')
    if len(wildParts) > 3:   #two many '*'
        return False
    #end if
    if len(wildParts) == 2:  # One '*'
        if wildCard == '*':
            return True
        #end if
        if wildParts[0] == '':  # '*fsdfdg'
            nChar = len(wildParts[1])
            if target[-nChar:] == wildParts[1]:
                return True
            else:
                return False
            #end if
        elif wildParts[1] == '': # 'lkjdg*'
            nChar = len(wildParts[0])
            if target[:nChar] == wildParts[0]:
                return True
            else:
                return False
            #end if
        else:
            nChar = len(wildParts[1])
            if target[-nChar:] == wildParts[1]:
                partTwoFlag = True
            else:
                partTwoFlag = False
            #end if
            nChar = len(wildParts[0])
            if target[:nChar] == wildParts[0]:
                partOneFlag = True
            else:
                partOneFlag = False
            #end if
            if partOneFlag and partTwoFlag:
                return True
            else:
                return False
            #end if
        #end if
    #end if
    if len(wildParts) == 3:  # Two '*'
        if wildParts[0] == '':
            partOneFlag = True
            nCharPartOne = 0
        else:
            nCharPartOne = len(wildParts[0])
            if target[:nCharPartOne] == wildParts[0]:
                partOneFlag = True
            else:
                partOneFlag = False
            #end if
        #end if
        if wildParts[2] == '':
            partThreeFlag = True
            nCharPartThree = 0
        else:
            nCharPartThree = len(wildParts[2])
            if target[-nCharPartThree:] == wildParts[2]:
                partThreeFlag = True
            else:
                partThreeFlag = False
            # end if
        # end if
        if wildParts[1] == '':
            partTwoFlag = True
            nCharPartTwo = 0
        else:
            print('                        wildParts[1]', wildParts[1])
            print('                        nCharPartOne',nCharPartOne)
            print('                      nCharPartThree',nCharPartThree)
            print('target[nCharPartOne:-nCharPartThree]', target[nCharPartOne:-nCharPartThree])
            endPos = len(target)-nCharPartThree
            if wildParts[1] in target[nCharPartOne:endPos]:
                partTwoFlag = True
            else:
                partTwoFlag = False
            # end if
        # end if
    else:
        partOneFlag = False
        partTwoFlag = False
        partThreeFlag = False
    #end if
    if partOneFlag and partTwoFlag and partThreeFlag:
        return True
    else:
        return False
    #end if
#end wiwildCardSearchString
def vSpace():
    return QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)
def hSpace():
    return QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
def iconsDir():
    thisFile = __file__
    # print('thisFile', thisFile)
    # thisFolder = os.path.dirname(inspect.getfile(tfuTools))
    parentFolder = os.path.dirname(thisFile)
    appFolder = os.path.dirname(parentFolder)
    iconFolder = os.path.join(appFolder, 'icons')
    return iconFolder

def makeButton(iconFileName=None, toolTip=None, objName=None, text=None, iconSize=32):
    iconFolder = iconsDir()

    btn = QPushButton()
    iconSize = iconSize * 1.0
    if toolTip != None:
        btn.setToolTip(toolTip)

    if iconFileName != None:
        icon = QIcon()
        iconFileName = os.path.join(iconFolder, iconFileName)
        icon.addPixmap(QPixmap(iconFileName), QIcon.Normal, QIcon.Off)
        btn.setIcon(icon)

    if objName != None:
        btn.setObjectName(objName)

    if text != None:
        btn.setText(text)
    #end if
    iconSize = int(iconSize)
    btn.setIconSize(QSize(iconSize, iconSize))

    btn.setEnabled(True)

    return btn
# end makeButton
