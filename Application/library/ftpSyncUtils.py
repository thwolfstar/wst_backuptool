from ftplib import FTP
import ftplib
import socket
import os
import time
import pickle
import datetime
import zipfile

# storageURL    = '192.168.0.150'
# userName     = 'admin'
# userPwd      = 'admin'
# timeOutParam = 600.0   #  This sets the socket time out


class FTPSync():
    def __init__(self, storageURL, userName, userPwd):
        self.userName   = userName
        self.userPwd    = userPwd
        self.storageURL = storageURL
        self.timeOutParam = 600.0 #  This sets the socket time out

        try:
            self.ftp = FTP(self.storageURL, timeout=self.timeOutParam)
            self.ftp.login(user=self.userName, passwd=self.userPwd)
            # self.dateTimeStrFmt = "%Y-%m-%d %H:%M:%S"
        except:
            print('Cannot connect to FTP server')
            print('Connection Timeout %2.1f sec' % self.timeOutParam)
            print(80 * '=')
            self.ftp = None
            return
        # end try
    #end __init__
    def putFile(self, localFile, remoteFile):
        # print('Uploading ',localFile,' to ',remoteFile)
        # print('')
        totalSize = os.path.getsize(localFile)
        uploadTracker = FtpUploadTracker(int(totalSize))
        notDone = True
        while notDone:
            try:
                fileName = localFile
                f = open(fileName, 'rb')  # local file to send
                # self.ftp.storbinary('STOR ' + remoteFile, f, 1024, uploadTracker.handle)  # send the file
                self.ftp.storbinary('STOR ' + remoteFile, f)  # send the file
                f.close()  # close the local file
                notDone = False
            except:
                f.close()
                self.ftp = FTP(self.storageURL, timeout=self.timeOutParam)
                self.ftp.login(user=self.userName, passwd=self.userPwd)
                notDone = True
            #end try
        #end while
    #end putFile
    def getFile(self, localFile, remoteFile):
        fileParts = remoteFile.split('/')
        remoteDir = '/'.join(fileParts[:-1])
        remoteFile = fileParts[-1]
        self.ftp.cwd(remoteDir)
        notDone = True
        while notDone:
            try:
                f = open(localFile, 'wb')
                self.ftp.retrbinary('RETR ' + remoteFile, f.write)  # get the remote file
                f.close()  # close the local file
                notDone = False
            except:
                f.close()
                self.ftp = FTP(self.storageURL, timeout=self.timeOutParam)
                self.ftp.login(user=self.userName, passwd=self.userPwd)
                notDone = True
            #end try
        #end while
        print('Retrieving file:', remoteFile,' as ',localFile)
        return True
    #end getFile
    def delFile(self, remoteFile, remoteDir = '.'):
        self.ftp.cwd(remoteDir)
        print('Deleting', remoteFile)
        if self.remoteFileExists(remoteFile, remoteDir):
            self.ftp.delete(remoteFile)
        else:
            print('File does not exist')
        #end if
        print('Done.')
    #end delFile
    def remoteDirExists(self, remoteDir='.'):
        dirParts = remoteDir.split('/')
        remoteParent = '/'.join(dirParts[:-1])
        dirOnly = dirParts[-1]
        try:
            resp = self.ftp.mlsd(remoteParent)
        except:
            return False
        #end try
        dirExistsFlag = False
        try:
            for dirItem in resp:
                # print(dirItem)
                # if dirItem[1]['type'] == 'dir':
                if dirItem[0] == dirOnly:
                    # print(' remoteDir',remoteDir)
                    # print('dirItem[0]',dirItem[0])
                    # print('   dirOnly',dirOnly)
                    dirExistsFlag = True
                    break
                #end if
                # #end if
            #end dirItem
        except:
            return False
        #end try
        return dirExistsFlag
    #end remoteDirExists
    def remoteFileExists(self,remoteFile):
        fileParts = remoteFile.split('/')
        remoteDir = '/'.join(fileParts[:-1])
        remoteFile = fileParts[-1]
        self.ftp.cwd(remoteDir)
        fList = self.ftp.mlsd(remoteDir)
        fileExistsFlag = False
        for line in fList:
            fName = line[0]
            # print(fName)
            if fName == remoteFile:
                fileExistsFlag = True
                break
            #end if
        #end line
        return fileExistsFlag
    #end remoteFileExists
    def testConnection(self):
        print('testConnect')

        notDone = True
        while notDone:
            try:
                print('>>> Testing Conection...')
                self.ftp.cwd('.')   # kitten ????
                # welcomeMessage = self.ftp.getwelcome()
                # print(welcomeMessage)
                notDone = False
            except:
                print('>>> ReConnecting...')
                try:
                    self.ftp = FTP(self.storageURL, timeout=self.timeOutParam)
                    self.ftp.login(user=self.userName, passwd=self.userPwd)
                    notDone = True
                except:
                    time.sleep(1)
                    notDone = True
                #end try
            #end try
    #end testConnection

    def rmTree(self, path):
        """Recursively delete a directory tree on a remote server.
            https://gist.github.com/artlogic/2632647

            Need to replace nlst with mlsd
            -- more robust - works with directories that start with '.'
        """

        nameInfos = list(self.ftp.mlsd(path))
        for nameInfo in nameInfos:
            name, infoDict = nameInfo
            # print(name, infoDict['type'])
        #end nameInfo

        for nameInfo in nameInfos:
            name, infoDict = nameInfo
            if infoDict['type'] in ('cdir', 'pdir'): continue

            # print('FtpRmTree: Checking {0}'.format(name))

            if infoDict['type'] == 'dir':
                # print(name)
                newPath = '%s/%s' % (path, name)
                # print('>>>', newPath)
                self.rmTree(newPath)
            else:
                fullPath = '%s/%s' % (path, name)
                # print('Del File:', fullPath)
                self.ftp.delete(fullPath)
            #end if
        #end nameInfos
        nameInfos = list(self.ftp.mlsd(path))
        if len(nameInfos) < 3:  # dir is empty -- just . and ..
            print('rmTree->ftp.rmd', path)
            self.ftp.rmd(path)

        #end if
    #end rmTree
#end class FTPSync
class FtpUploadTracker:
    sizeWritten = 0
    totalSize = 0
    lastShownPercent = 0

    def __init__(self, totalSize):
        self.totalSize = totalSize

    def handle(self, block):
        self.sizeWritten += 1024
        percentComplete = round((self.sizeWritten / self.totalSize) * 100)

        if (self.lastShownPercent != percentComplete):
            self.lastShownPercent = percentComplete
            print(str(percentComplete) + " percent complete", end='\r')
        #end if
    #end handle
#end class FtpUploadTracker

