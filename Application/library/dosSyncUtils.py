import os
import shutil

class DOSSync():
    def __init__(self, storagePath,):
        self.storagePath = storagePath
    #end __init__
    def putFile(self, localFile, remoteFile):
        # print('Uploading ',localFile,' to ',remoteFile)
        # print('')
        totalSize = os.path.getsize(localFile)
        shutil.copyfile(localFile, remoteFile)
    #end putFile
    def getFile(self, localFile, remoteFile):
        print('Retrieving file:', remoteFile,' as ',localFile)
        shutil.copyfile(remoteFile, localFile)
        return True
    #end getFile
    def delFile(self, remoteFile):
        print('Deleting', remoteFile)
        if os.path.exists(remoteFile):
            os.remove(remoteFile)
        else:
            print('File does not exist')
        #end if
        print('Done.')
    #end delFile
    def remoteDirExists(self, remoteDir='.'):
        if os.path.exists(remoteDir):
            if os.path.isdir(remoteDir):
                return True
            else:
                return False
            #end if
        else:
            return False
        #end if
    #end remoteDirExists
    def remoteFileExists(self,remoteFile):
        if os.path.exists(remoteFile):
            if os.path.isfile(remoteFile):
                return True
            else:
                return False
            #end if
        else:
            return False
        #end if
    #end remoteFileExists
    def rmTree(self, path):
        if os.path.exists(path):
            if os.path.isdir(path):
                shutil.rmtree(path)
            #end if
        #end if
    #end rmTree


#end class DOSSync