import platform
import datetime
import os
import pickle
import sys
import shutil
import glob
import time
import webbrowser
# import inspect
from library import FTPSync
from send2trash import send2trash

from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QPixmap


from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QAbstractItemView

from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QTableWidget
from PyQt5.QtWidgets import QTableWidgetItem
from PyQt5.QtWidgets import QTreeWidget
from PyQt5.QtWidgets import QTreeWidgetItem
from PyQt5.QtWidgets import QAbstractItemView

from PyQt5.QtWidgets import QProgressBar

from library.wstTools import hSpace
from library.wstTools import vSpace
from library.wstTools import wildCardSearchString
from library.wstTools import iconsDir
from library.wstTools import makeButton


#
#  User Defaults
#
#
#  First Time Through Data
#
# userDefConnectionType = 'FTP'
# userDefstorageUrl     = '192.168.0.108'
# userDefuserName       = 'admin'
# userDefuserPwd        = 'admin'
# userDefremoteRootDir  = '/'.join(['/array1', 'Share', 'WST_BackUp'])

userDefConnectionType = 'DOS'
userDefstorageUrl     = ''
userDefuserName       = ''
userDefuserPwd        = ''
if platform.node() == 'WST11':
    userDefremoteRootDir  = 'Z:\\'
elif platform.node() == 'WST12':
    userDefremoteRootDir = 'F:\\WST_Backup\\'
#end if

myInfoFile = 'WST_backUpInfo.bin'
myFolderFile = 'WST_backupFolders.bin'

class IncrementalBackUp():
    def __init__(self,localDir):
        self.nodeName = platform.node()
        self.infoDir = r'C:\scratch\WST_BackUpTool\WST_BackupInfo'
        self.infoDict = {}
        self.backupFiles = []


        #
        #  Items Populated by self.getRunInfo()
        #
        # self.storageURL = '192.168.0.150'
        # self.userName = 'admin'
        # self.userPwd = 'admin'
        # self.remoteRootDir = '/'.join(['/array1', 'Share', 'WST_BackUp'])
        # self.modDate = datetime.datetime(2020, 11, 16)
        # self.infoDict = {}

        self.localDir = localDir
        self.infoFile = os.path.join(self.infoDir,myInfoFile)
        self.getRunInfo()

        self.skipFiles = ['_upg.odb', '.stt', 'file0.rst', 'file1.rst', 'WST_tempVTFX', 'wst_usage.log', '-LogFile.txt',
                          'WST_backupFolders.bin', 'WST_backUpInfo.bin', 'RunLog.txt', '~']

        localParts = localDir.split(os.path.sep)
        if ':' in localParts[0]:
            driveLetter = localParts[0]
            driveLetter = driveLetter.replace(':','')
            localParts[0] = driveLetter
        #end if
        self.remoteDir = '-'.join(localParts)
        self.remoteDir = '%s %s' % (self.nodeName, self.remoteDir)

        self.totalSize = 0
        self.fileCount = 0

        self.backupDateTime = datetime.datetime.now()

        self.dateStr = self.backupDateTime.strftime('%Y-%m-%d_%HH%MM')
        self.incDir = '%s Incremental' % (self.dateStr)
        if self.connectionType == 'FTP':
            self.remoteDir = '/'.join([self.remoteRootDir, self.remoteDir, self.incDir])
        elif self.connectionType == 'DOS':
            self.remoteDir = os.path.join(self.remoteRootDir, self.remoteDir, self.incDir)
        #end if

        # print(self.storageURL, self.userName, self.userPwd)
        self.ftpHost = None
        if self.connectionType == 'FTP':
            self.ftpHost = FTPSync(self.storageURL, self.userName, self.userPwd)
        elif self.connectionType == 'DOS':
            self.ftpHost = None
        #end if
    #end __init__
    def makeTree(self):
        print(' '.join('Creating Remote Directory Structure...').center(80))
        # print('makeTree:', self.remoteDir)
        # print('makeTree:', self.localDir)
        # print('dir(self.ftpHost.ftp)',dir(self.ftpHost.ftp))
        # print('type(self.ftpHost.ftp)',type(self.ftpHost.ftp))
        # print('type(self.ftpHost)',type(self.ftpHost))
        self.ftpHost.ftp.mkd(self.remoteDir)
        # traverse root directory, and list directories as dirs and files as files
        # if os.path.exists(self.localDir):
        #     print('its there')
        # else:
        #     print('its not there')
        # #end if
        for root, dirs, files in os.walk(self.localDir):
            path = root.split(os.sep)
            # print(root)
            noDrive = os.path.splitdrive(root)[1]
            noDrive = noDrive.split('\\')

            remoteRoot = '/'.join([self.remoteDir]+noDrive)
            # print('makeTree:',remoteRoot)
            self.ftpHost.ftp.mkd(remoteRoot)
            # print((len(path) - 1) * '---', os.path.basename(root))
            # for file in files:
            #     print(len(path) * '---', file)
        #end root
    #end makeTree
    def scanSize(self):
        print(' '.join('Scanning Backup Size...').center(80))
        self.totalSize = 0
        self.fileCount = 0
        for root, dirs, files in os.walk(self.localDir):
            path = root.split(os.sep)
            # print((len(path) - 1) * '---', os.path.basename(root))
            for file in files:
                # print(len(path) * '---', file)
                skipFlag = False
                for skipFile in self.skipFiles:
                    if skipFile in file:
                        skipFlag = True
                        break
                    #end if
                #end skipFile
                if skipFlag:
                    continue
                #end if

                fullPath = os.path.join(root, file)
                fileModTime = os.path.getmtime(fullPath)
                fileModTime = datetime.datetime.fromtimestamp(fileModTime)

                if fileModTime > self.modDate:
                    # print('scanSize:', self.modDate, fileModTime, fullPath)
                    fileSize = os.path.getsize(fullPath)
                    self.totalSize = self.totalSize + fileSize
                    self.fileCount += 1
                    self.backupFiles.append(fullPath)
                #end if
            #end file
        # end root
        return
    #end scanSize
    def storeBackup(self):
        print(' '.join('Storing Backup...').center(80))
        print(80*'-')
        logFileName = '%s-LogFile.txt' % self.dateStr
        logFileName = os.path.join(self.infoDir, logFileName)
        f=open(logFileName, 'a')
        totalSizeMb = self.totalSize / (1024 * 1024)
        ktr = 0
        sizeSum = 0
        for root, dirs, files in os.walk(self.localDir):
            path = root.split(os.sep)
            # print((len(path) - 1) * '---', os.path.basename(root))
            for file in files:
                # print(len(path) * '---', file)
                skipFlag = False
                for skipFile in self.skipFiles:
                    if skipFile in file:
                        skipFlag = True
                        break
                    #end if
                #end skipFile
                if skipFlag:
                    continue
                #end if
                fullPath = os.path.join(root, file)
                fileModTime = os.path.getmtime(fullPath)
                fileModTime = datetime.datetime.fromtimestamp(fileModTime)
                if fileModTime > self.modDate:
                    # print(self.modDate, fileModTime, fullPath)
                    fileSize = os.path.getsize(fullPath)
                    fileSizeMb = fileSize / (1024 * 1024)
                    sizeSum = sizeSum + fileSize
                    ktr += 1
                    remotePath = os.path.splitdrive(fullPath)[1]
                    # print('** Before remotePath', remotePath)
                    if self.connectionType == 'FTP':
                        remotePath = remotePath.split(os.sep)
                        remotePath = '/'.join([self.remoteDir]+remotePath)
                    elif self.connectionType == 'DOS':
                        remoteDrive = os.path.splitdrive(self.remoteDir)[0]
                        remoteRoot = os.path.splitdrive(self.remoteDir)[1]
                        pathList = [remoteDrive] + remoteRoot.split(os.sep) + remotePath.split(os.sep)
                        # print('pathList',pathList)
                        remotePath = os.path.join(*pathList)
                        remotePath = remotePath.replace(':',':\\')
                    #end if
                    # print('self.remoteDir', self.remoteDir)
                    # print('remotePath', remotePath)

                    # print(ktr,'of',self.fileCount)
                    # print(sizeSum,'of',self.totalSize)
                    if self.totalSize != 0.0:
                        sizePer = (sizeSum / self.totalSize) * 100.0
                    else:
                        sizePer = 0.0
                    #end if
                    sizeSumMb = sizeSum / (1024 * 1024)
                    prinTxt = 'Storing File %04i of %04i (%10.3f MB): Total %4.3f of %4.3f MB (%7.3f%%): %80s' % (ktr, self.fileCount, fileSizeMb, sizeSumMb, totalSizeMb,  sizePer, remotePath[-80:])
                    # print(prinTxt, end='\r')
                    print(prinTxt)
                    # print('storeBackup:',remotePath)

                    # print('fullPath',fullPath)
                    # print('remotePath',remotePath)

                    if self.connectionType =='FTP':
                        self.ftpHost.putFile(fullPath, remotePath)
                    elif self.connectionType =='DOS':
                        parDir = os.path.dirname(remotePath)
                        if not os.path.exists(parDir): os.makedirs(parDir)
                        try:
                            shutil.copyfile(fullPath, remotePath)
                        except:
                            print('Could not copy',fullPath, 'to', remotePath)
                        #end try
                    #end if
                    textLine = '%s %i bytes %s \n' % (str(fileModTime), fileSize, fullPath)
                    f.write(textLine)
                #end if
            #end file
        # end root
        f.close()
        return
    #end storeBackup
    def runIncrementalBackup(self):
        print(80 * '=')

        self.scanSize()
        totalSizeMB = self.totalSize / (1024 * 1024)
        printTxt = ' Backup Total Size: %4.3f MB' % totalSizeMB
        print(printTxt)
        print('Backup Total Files:', self.fileCount)


        sys.stdout.flush()
        if self.fileCount == 0:
            print(' '.join('No files to backup.  Skipping...').center(80))
            print(80 * '=')
            return
        else:
            print(self.remoteDir)
            curDir = os.path.abspath('.')
            parDir = os.path.dirname(self.remoteDir)
            remoteDirNotFound = True
            ktr = 0
            while remoteDirNotFound and ktr<20:
                # print('self.remoteDir',self.remoteDir)
                # print('curDir',curDir)
                # print('parDir',parDir, os.path.exists(parDir))
                # print('remoteDirNotFound',remoteDirNotFound)
                # print('ktr',ktr)


                try:
                    print('Testing remote dir')
                    # osCmd = 'explorer z:'
                    # os.system(osCmd)
                    # webbrowser.open(r'\\WST12\WST_Backup', autoraise=False)
                    # webbrowser.open('Z:', autoraise=False)
                    if platform.node() == 'WST11':
                        os.chdir('Z:')
                    elif platform.node() == 'WST12':
                        os.chdir('F:')
                    #end if
                    remoteDirNotFound = False
                except:
                    print('Failed, pausing...')
                    time.sleep(2)
                    remoteDirNotFound = True
                #end try
                ktr += 1
            #end while
            os.chdir(parDir)
            os.mkdir(self.remoteDir)
            os.chdir(curDir)
        # end if

        print(80 * '-')

        if self.connectionType=='FTP': self.makeTree()

        print(80 * '-')
        sys.stdout.flush()

        self.storeBackup()
        print(80 * '-')
        sys.stdout.flush()

        self.storeBackupFolderList()
        print(80 * '-')
        sys.stdout.flush()


        self.storeRunInfo()
        print(80 * '=')
        sys.stdout.flush()



    #end runIncrementalBackup
    def storeRunInfo(self):
        print(' '.join('Storing Backup Info...').center(80))
        self.infoDict[self.localDir] = {}
        self.infoDict[self.localDir]['modDate']        = self.backupDateTime  # next time we run this will be the modDate
        self.infoDict[self.localDir]['connectionType'] = self.connectionType  # next time we run this will be the modDate
        self.infoDict[self.localDir]['storageURL']     = self.storageURL  # next time we run this will be the modDate
        self.infoDict[self.localDir]['remoteRootDir']  = self.remoteRootDir  # next time we run this will be the modDate
        self.infoDict[self.localDir]['username']       = self.userName  # next time we run this will be the modDate
        self.infoDict[self.localDir]['userPwd']        = self.userPwd  # next time we run this will be the modDat

        f=open(self.infoFile, 'wb')
        pickle.dump(self.infoDict, f)
        f.close()
    #end storeRunInfo
    def getRunInfo(self):
        if os.path.exists(self.infoFile):
            f=open(self.infoFile, 'rb')
            self.infoDict = pickle.load(f)
            f.close()
        else:
            self.infoDict = {}
        #end if
        print('Info File:',self.infoFile)
        # print(self.infoDict.keys())
        if self.localDir in self.infoDict.keys():
            # print(self.infoDict[self.localDir].keys())
            modDate = self.infoDict[self.localDir]['modDate']
            status = 'DEFINED'
        else:
            status = 'UNDEFINED'
        #end if

        if status == 'UNDEFINED':
            modDate = datetime.datetime.now()
            modDate = modDate - datetime.timedelta(weeks=4.0)
        #end if
        self.modDate = modDate

        if status == 'DEFINED':
            self.connectionType = self.infoDict[self.localDir]['connectionType']
            self.storageURL     = self.infoDict[self.localDir]['storageURL']
            self.remoteRootDir  = self.infoDict[self.localDir]['remoteRootDir']
            self.userName       = self.infoDict[self.localDir]['username']
            self.userPwd        = self.infoDict[self.localDir]['userPwd']
        else:
            self.connectionType = userDefConnectionType
            self.storageURL     = userDefstorageUrl
            self.remoteRootDir  = userDefremoteRootDir
            self.userName       = userDefuserName
            self.userPwd        = userDefuserPwd
        #end
        # self.storageURL     = userDefstorageUrl
    #end getRunInfo
    def storeBackupFolderList(self):
        print(' '.join('Storing Backup Folder List...').center(80))
        print(80*'-')
        remoteDir = self.remoteDir
        remoteParts = remoteDir.split(os.path.sep)
        backupJob = remoteParts[-1]
        backupSet = remoteParts[-2]
        print('   Back Up Set:', backupSet)
        print('   Back Up Job:', backupJob)
        print('Back Up Folder:', self.remoteDir)
        sys.stdout.flush()

        self.storeFolderList(backupJob, backupSet)
    #end storeFolderList
    def storeFolderList(self, backupJob, backupSet):
        # print('backupJob',backupJob)
        # print('backupSet',backupSet)
        #
        #  Get list of folders
        #
        folderFile = os.path.join(self.infoDir, myFolderFile)
        # print('folderFile', folderFile)
        # print('self.backupSet', self.backupSet)
        # print('backupFolder', backupFolder)
        setKey = backupSet.split('/')[-1]
        folderKey = backupJob
        # print('setKey', setKey)
        # print('folderKey', folderKey)

        # backupFolder = '%s/%s/%s' % (self.rootDir, backupSet, backupJob)
        backupFolder = self.remoteDir

        folderFileOKFlag = False
        folderDict = {}
        if os.path.exists(folderFile):
            f=open(folderFile, 'rb')
            folderDict = pickle.load(f)
            f.close()
            if setKey in folderDict.keys():
                if folderKey in folderDict[setKey].keys():
                    folderFileOKFlag = True
                #end if
            #end if
        #end if

        if folderFileOKFlag:
            folderList = folderDict[setKey][folderKey]
        else:
            folderList = self.getTree(backupFolder)
            #
            #  Save the folderList so we don't have to do this next time
            #
            if setKey not in folderDict.keys():
                folderDict[setKey] = {}
            #end if
            folderDict[setKey][folderKey] = folderList
            f=open(folderFile, 'wb')
            pickle.dump(folderDict, f)
            f.close()
        #end if

        return folderList
    #end storeFolderList
    def getTree(self, targetDir):
        print('targetDir:',targetDir)
        treeList = []
        if self.connectionType == 'FTP':
            self.ftpHost.ftp.cwd(targetDir)
            fileInfos = self.ftpHost.ftp.mlsd()
            for fileInfo in fileInfos:
                name, infoDict = fileInfo
                if infoDict['type'] == 'dir':
                    # print(targetDir, name, infoDict['type'])
                    treeList.append(name)
                    newTarget = '%s/%s' % (targetDir, name)
                    # print('newTarget',newTarget)
                    newBranch = self.getTree(newTarget)
                    if newBranch != []:
                        treeList.append(newBranch)
                    #end if
                #end if
            #end fileInfo
        elif self.connectionType == 'DOS':
            for root, dirs, files in os.walk(targetDir):
                # print(root)
                treeList.append(root)
            # end walk
        #end if
        return treeList
    #end getTree
#end class IncrementalBackUp
class RestoreTool(QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowFlags(self.windowFlags()|  QtCore.Qt.WindowMinimizeButtonHint |
        QtCore.Qt.WindowSystemMenuHint )
        # self.rootDir = '/array1/Share/WST_BackUp'
        self.infoDir = r'C:\scratch\WST_BackUpTool\WST_BackupInfo'
        self.infoFile = os.path.join(self.infoDir, myInfoFile)
        self.localDir = r'C:\scratch'
        self.getRunInfo()

        self.backupSets = []
        self.branchPath = ''





        # self.infoDict = {}

        nodeName = platform.node()

        if self.connectionType == 'FTP': self.ftpHost = FTPSync(self.storageURL, self.userName, self.userPwd)

        self.backupJobs = []
        self.setUpUI()
    #end __init__
    def setUpUI(self):

        #
        #  Way back Graphic
        #
        self.wayBackPic = QLabel()
        iconFolder = iconsDir()
        iconFileName = 'wayBackMachine_128x128.png'
        iconFileName = os.path.join(iconFolder, iconFileName)
        # print(os.path.exists(iconFileName))
        # print(os.path.abspath(iconFileName))
        icon = QIcon()
        # iconFileName = os.path.join(iconFolder, iconFileName)
        self.wayBackPic.setPixmap(QPixmap(iconFileName))
        # self.wayBackPic.show()

        #
        #  Info File Selection
        #
        self.infoFileLbl = QLabel('Info File:')
        self.infoFileLE  = QLineEdit(self.infoFile)
        self.infoFileLE.setMinimumWidth(400)
        self.browseInfoFileBtn = makeButton(text='Browse')

        #
        #  Backup Sets Combo
        #
        self.getBackupSets()
        self.backupSetsCombo = QComboBox()
        self.backupSetsCombo.addItems(self.backupSets)
        self.backupSetsCombo.currentIndexChanged.connect(self.fillBackupJobTable)

        #
        #  Generate Folder Data
        #
        self.genFolderDataBtn = makeButton(iconFileName='icon_genFolderTrees_64x64.png', text='Gen Folder Data', toolTip='Generate folder structure\nWill be slow')
        self.genFolderDataBtn.clicked.connect(self.slotGenFolderData)

        #
        #  Compress Time
        #
        self.timeCompressBtn = makeButton(iconFileName='icon_timeCompression_64x64.jpg', text='Compress Time', toolTip='Compress Time')
        self.timeCompressBtn.clicked.connect(self.slotCompressTime)

        #
        #  File Search
        #
        self.fileSearchLbl = QLabel('File Search:')
        self.fileSearchLE = QLineEdit()
        self.fileSearchLE.setMaximumWidth(200)
        self.fileSearchBtn = makeButton(text='Search')
        self.fileSearchBtn.clicked.connect(self.slotFileSearch)

        # #
        # #  Busy GIF
        # #
        # self.busyLbl = QLabel()
        # self.busyMovie = QMovie(r"C:\scratch\WST_BackUpTool\Application\icons\busy.gif")
        # self.busyLbl.setMovie(self.busyMovie)
        # self.busyLbl.hide()
        # # self.busyMovie.start()

        #
        #  Progress Bar
        #
        self.progBar =QProgressBar()
        self.progBar.hide()

        #
        #  Progress Label
        #
        self.progLbl = QLabel()
        self.progLbl.hide()

        #
        #  Backup Jobs Table
        #
        self.backupJobsTable = QTableWidget()
        self.backupJobsTable.setMinimumSize(QSize(100, 150))
        self.backupJobsTable.setMaximumWidth(300)
        # self.backupJobsTable.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.backupJobsTable.setRowCount(0)
        self.backupJobsTable.setColumnCount(1)
        item = QTableWidgetItem()
        self.backupJobsTable.setHorizontalHeaderItem(0, item)
        item = self.backupJobsTable.horizontalHeaderItem(0)
        item.setText('Backup Job')
        self.backupJobsTable.setSelectionMode(QAbstractItemView.SingleSelection)

        self.backupJobsTable.currentItemChanged.connect(self.fillFolderTree)

        #
        #  Folder Tree
        #
        self.folderTree = QTreeWidget()
        self.folderTree.setMaximumWidth(300)
        self.folderTree.setHeaderLabels(['Folder'])
        self.folderTree.setAlternatingRowColors(True)
        self.folderTree.itemSelectionChanged.connect(self.populateFiles)
        self.fillFolderTree()

        #
        #  Backup Files Table
        #
        self.fileTable = QTableWidget()
        self.fileTable.setMinimumSize(QSize(100, 150))
        self.fileTable.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.fileTable.setSortingEnabled(True)
        self.fileTable.setRowCount(0)
        self.fileTable.setColumnCount(5)

        item = QTableWidgetItem()
        self.fileTable.setHorizontalHeaderItem(0, item)
        item = self.fileTable.horizontalHeaderItem(0)
        item.setText('File')

        item = QTableWidgetItem()
        self.fileTable.setHorizontalHeaderItem(1, item)
        item = self.fileTable.horizontalHeaderItem(1)
        item.setText('Ext')

        item = QTableWidgetItem()
        self.fileTable.setHorizontalHeaderItem(2, item)
        item = self.fileTable.horizontalHeaderItem(2)
        item.setText('Mod Date (UTC)')

        item = QTableWidgetItem()
        self.fileTable.setHorizontalHeaderItem(3, item)
        item = self.fileTable.horizontalHeaderItem(3)
        item.setText('Size (KB)')

        item = QTableWidgetItem()
        self.fileTable.setHorizontalHeaderItem(4, item)
        item = self.fileTable.horizontalHeaderItem(4)
        item.setText('Backup Job')

        self.fillBackupJobTable()

        #
        # Restore Button
        #
        self.restoreBtn = makeButton(iconFileName='icon_restore_64x64.png', text='Restore')
        self.restoreBtn.clicked.connect(self.slotRestore)
        # =========================================
        #  Layout Widgets
        # =========================================

        mainLayout = QVBoxLayout()
        self.setLayout(mainLayout)

        self.leftColLayout = QVBoxLayout()
        self.rightColLayout = QVBoxLayout()
        self.topStuffLayout = QHBoxLayout()

        mainLayout.addLayout(self.topStuffLayout)
        self.topStuffLayout.addLayout(self.leftColLayout)
        self.topStuffLayout.addLayout(self.rightColLayout)

        infoLayout = QHBoxLayout()
        self.leftColLayout.addLayout(infoLayout)
        backupSetsLayout = QHBoxLayout()
        progBarLayout = QHBoxLayout()
        self.leftColLayout.addLayout(backupSetsLayout)

        restoreInfoLayout = QHBoxLayout()
        mainLayout.addLayout(restoreInfoLayout)

        infoLayout.addWidget(self.infoFileLbl)
        infoLayout.addWidget(self.infoFileLE)
        infoLayout.addWidget(self.browseInfoFileBtn)

        backupSetsLayout.addWidget(QLabel('Backup Set:'))
        backupSetsLayout.addWidget(self.backupSetsCombo)
        backupSetsLayout.addWidget(self.genFolderDataBtn)
        backupSetsLayout.addWidget(self.timeCompressBtn)
        backupSetsLayout.addItem(hSpace())
        backupSetsLayout.addWidget(self.fileSearchLbl)
        backupSetsLayout.addWidget(self.fileSearchLE)
        backupSetsLayout.addWidget(self.fileSearchBtn)

        # backupSetsLayout.addItem(hSpace())

        self.leftColLayout.addLayout(progBarLayout)
        # progBarLayout.addWidget(self.busyLbl)
        progBarLayout.addWidget(QLabel(' '))
        progBarLayout.addWidget(self.progBar)
        self.leftColLayout.addWidget(self.progLbl)

        restoreInfoLayout.addWidget(self.backupJobsTable)
        restoreInfoLayout.addWidget(self.folderTree)
        restoreInfoLayout.addWidget(self.fileTable)
        restoreInfoLayout.addWidget(self.fileTable)

        mainLayout.addWidget(self.restoreBtn)
        self.rightColLayout.addWidget(self.wayBackPic)

    #end setUpUI
    def getBackupSets(self):



        if self.connectionType == 'FTP':
            self.rootDir = '/disk1/share/WST_Backup'
            if platform.node() == 'WST11':
                self.rootDir = '/array1/Share/WST_BackUp'
            else:
                self.rootDir = '/disk1/share/WST_Backup'
            #end if
            self.ftpHost.ftp.cwd(self.rootDir)
            self.backupSets = self.ftpHost.ftp.nlst()
        elif self.connectionType == 'DOS':
            self.rootDir = 'Z:\\'
            os.chdir(self.rootDir)
            self.backupSets = glob.glob('WST*')
        #end if
        print(self.rootDir)

        for backupSet in self.backupSets:
            print(backupSet)
        #end backupSet
    #end getBackupSets
    def getBackupJobs(self):
        # print(self.rootDir)
        self.ftpHost.ftp.cwd(self.rootDir)
        self.backupJobs = self.ftpHost.ftp.nlst()
    #end getBackupJobs
    def storeFolderList(self, backupJob, backupSet):
        # print('backupJob',backupJob)
        # print('backupSet',backupSet)
        #
        #  Get list of folders
        #
        folderFile = os.path.join(self.infoDir, myFolderFile)
        # print('folderFile', folderFile)
        # print('self.backupSet', self.backupSet)
        # print('backupFolder', backupFolder)
        setKey = backupSet.split('/')[-1]
        folderKey = backupJob
        # print('setKey', setKey)
        # print('folderKey', folderKey)

        if self.connectionType == 'FTP':
            backupFolder = '%s/%s/%s' % (self.rootDir, backupSet, backupJob)
        elif self.connectionType == 'DOS':
            backupFolder = os.path.join(self.rootDir, backupSet, backupJob)
        #end if


        folderFileOKFlag = False
        folderDict = {}
        if os.path.exists(folderFile):
            f=open(folderFile, 'rb')
            folderDict = pickle.load(f)
            f.close()
            if setKey in folderDict.keys():
                if folderKey in folderDict[setKey].keys():
                    folderFileOKFlag = True
                #end if
            #end if
        #end if

        if folderFileOKFlag:
            folderList = folderDict[setKey][folderKey]
        else:
            folderList = self.getTree(backupFolder)
            #
            #  Save the folderList so we don't have to do this next time
            #
            if setKey not in folderDict.keys():
                folderDict[setKey] = {}
            #end if
            folderDict[setKey][folderKey] = folderList
            f=open(folderFile, 'wb')
            pickle.dump(folderDict, f)
            f.close()
        #end if

        return folderList

    #end storeFolderList
    def slotGenFolderData(self):
        print(80*'=')
        print(' '.join('Generating Folder Structures...').center(80))
        print(80*'-')
        backupSet = self.backupSetsCombo.currentText()

        nJobs = self.backupJobsTable.rowCount()
        self.progBar.setMaximum(nJobs)
        self.progBar.show()
        for iJob in range(nJobs):

            backupJobItem = self.backupJobsTable.item(iJob,0)
            if backupJobItem is None:
                return
            #end if
            backupJob = backupJobItem.text()
            if backupJob == '':
                continue
            #end if
            self.progBar.setValue(iJob)
            self.progBar.setFormat(backupJob+' %p%')
            self.progBar.setAlignment(Qt.AlignCenter)
            print('Processing Backup Job:', backupJob)
            print('Processing Backup Set:',backupSet)
            QApplication.processEvents()

            folderList = self.storeFolderList(backupJob, backupSet)

            print(80*'-')
        #end iJob
        print(80 * '=')
        self.progBar.setFormat('%p%')
        self.progBar.hide()
    #end slogGenFolderData
    def fillFolderTree(self):
        backupJobItem = self.backupJobsTable.currentItem()
        if backupJobItem is None:
            return
        #end if
        backupJob = backupJobItem.text()
        if backupJob == '':
            return
        #end if

        self.folderTree.blockSignals(True)
        self.folderTree.clear()

        # backupJob = self.backupJobs[-1]
        backupSet = self.backupSetsCombo.currentText()
        backupFolder = '%s/%s/%s' % (self.rootDir, backupSet, backupJob)

        print('Populating Tree:', backupSet,backupJob)
        folderList = self.storeFolderList(backupJob, backupSet)
        print('fillFolder->folderList',folderList)

        self.populateTree(self.folderTree, folderList)
        self.folderTree.blockSignals(False)

        self.fileTable.blockSignals(True)
        while self.fileTable.rowCount() > 0:
            self.fileTable.removeRow(0)
        #end while
        self.fileTable.blockSignals(False)

    #end fillFolderTree
    def populateTree(self, parentTree, folderList):
        if self.connectionType == 'FTP':
            for folder in folderList:
                if type(folder) == type('string'):
                    folderBranch = QTreeWidgetItem(parentTree, [folder])
                else:
                    self.populateTree(folderBranch, folder)
                #end if
            #end folder
        elif self.connectionType == 'DOS':
            topFolder = folderList[1]
            topItem = QTreeWidgetItem(self.folderTree, [os.path.basename(topFolder)])
            self.folderTree.addTopLevelItem(topItem)
            self.walkTree(topFolder, topItem)
        #end if
    #end populateTree
    def walkTree(self, topFolder, topItem):
        treeItemDict = {topFolder: topItem}
        for root, folders, files in os.walk(topFolder):
            if root != topFolder:  # topFolder added as topLevelItem to QTreeWidget already
                # add folders
                parentPath = os.path.split(root)[0]
                parentItem = treeItemDict[parentPath]
                childItem = self.addTreeFolder(root, parentItem)
                treeItemDict[root] = childItem
            # end if
        # next root
    #end walkTree
    def addTreeFolder(self, root, parentItem):
        child = QTreeWidgetItem()
        baseRoot = os.path.basename(root)
        child.setText(0, baseRoot)
        parentItem.addChild(child)
        return child
    # end addTreeFolder
    def getBranch(self):
        print(40*'-')
        item = self.folderTree.currentItem()
        if item is None:
            self.folderTree.blockSignals(True)
            self.folderTree.setCurrentItem(self.folderTree.topLevelItem(0))
            self.folderTree.blockSignals(False)
            item = self.folderTree.currentItem()
        #end if
        # print('\n'.join(dir(item)))
        # print(item.text(0))
        parent = item.parent()
        branchList = []
        branchList.append(item.text(0))
        while parent is not None:
            # print(parent.text(0))
            branchList = [parent.text(0)] + branchList
            parent = parent.parent()
        #end while
        # print(branchList)
        self.branchPath = '/'.join(branchList)
        # print(self.branchPath)

    #end getBranch
    def slotFileSearch(self):
        print('slotFileSearch')
        wildCardTxt = self.fileSearchLE.text()
        if wildCardTxt == '':
            return
        #end if
        startJobRow = self.backupJobsTable.currentRow()
        print('startJobRow', startJobRow)
        if startJobRow == -1:
            self.backupJobsTable.selectRow(0)
            self.folderTree.blockSignals(True)
            self.folderTree.setCurrentItem(self.folderTree.topLevelItem(0))
            self.folderTree.blockSignals(False)
        #end if
        startJobRow = self.backupJobsTable.currentRow()
        print('startJobRow', startJobRow)
        self.getBranch()

        while self.fileTable.rowCount() > 0:
            self.fileTable.removeRow(0)
        #end while
        backupSet = self.backupSetsCombo.currentText()
        startRow = self.backupJobsTable.currentRow()
        lastRow = self.backupJobsTable.rowCount()
        nRows = lastRow - startRow
        self.progBar.setMaximum(nRows)
        self.progBar.setTextVisible(False)
        ktr = 0
        self.progBar.show()
        self.progBar.setValue(nRows)
        fullList = []
        print('backupSet', backupSet)
        for iRow in range(startRow, lastRow):
            currentItem = self.backupJobsTable.item(iRow,0)
            currentJob = currentItem.text()
            print('currentJob', currentJob)
            currentBranch = '%s/%s/%s/%s' % (self.rootDir, backupSet,currentJob, self.branchPath)
            # print('currentBranch', currentBranch)
            leafList = self.getLeaves(currentBranch, [])
            for leafInfos in leafList:
                # print(leafInfos, dir(leafInfos))
                fullName, info = leafInfos
                name, infoDict = info
                # print(fullName, name, infoDict['type'])
                if wildCardSearchString(name, wildCardTxt):
                    if leafInfos not in fullList:
                        fullList.append(leafInfos)
                    #end if
                #end if
                # for leafInfo in leafInfos:
                #     print(leafInfo)
                # #end if
            #end leafInfo
            ktr += 1
            self.progBar.setValue(ktr)
            self.progBar.setTextVisible(True)
            QApplication.processEvents()
        #end iRow
        self.progBar.hide()
        # print('\n'.join(leafList))
        checkList = []
        for leafInfos in fullList:
            fullName, info = leafInfos
            name, infoDict = info
            # print(fullName, name, infoDict['type'])
            # print(infoDict.keys())
            mod = infoDict['modify']
            fileSize = infoDict['size']
            fileSize = int(fileSize) / 1024
            yearTxt = mod[:4]
            moTxt = mod[4:6]
            dayTxt = mod[6:8]
            hrTxt = mod[8:10]
            minTxt = mod[10:12]
            secTxt = mod[12:14]
            # print(mod)
            mod = '%s-%s-%s_%s:%s:%s' % (yearTxt, moTxt, dayTxt, hrTxt, minTxt, secTxt)
            #0123 45 67 89 01 23
            #2020 11 17 12 05 47
            # print(mod, fileSize, type(mod), type(fileSize))
            fileSize = '%1.3f' % fileSize
            fullParts = fullName.split('/')
            backUpJob = fullParts[5]
            fullName = '/'.join(fullParts[6:])
            if fullName in checkList:
                continue
            #end if
            checkList.append(fullName)
            # print('backUpJob',backUpJob)
            # print('fullName', fullName)
            fileExt = os.path.splitext(fullName)[-1]

            nRows = self.fileTable.rowCount()
            self.fileTable.insertRow(nRows)
            self.fileTable.setItem(nRows, 0, QTableWidgetItem(fullName))
            self.fileTable.setItem(nRows, 1, QTableWidgetItem(fileExt))
            self.fileTable.setItem(nRows, 2, QTableWidgetItem(mod))
            self.fileTable.setItem(nRows, 3, QTableWidgetItem(fileSize))
            self.fileTable.setItem(nRows, 4, QTableWidgetItem(backUpJob))

        #end leafInfos

    #end slotFileSearch
    def populateFiles(self):
        self.getBranch()
        while self.fileTable.rowCount() > 0:
            self.fileTable.removeRow(0)
        #end while
        # print('self.rootDir',self.rootDir)
        backupSet = self.backupSetsCombo.currentText()
        # print('backupSet',backupSet)
        # print('self.branchPath',self.branchPath)

        startJobRow = self.backupJobsTable.currentRow()
        nBackupJobs = self.backupJobsTable.rowCount()
        nJobs = nBackupJobs - startJobRow - 1
        ktr = 0
        self.progBar.setMaximum(nJobs)
        self.progBar.show()
        fileList = []
        for itemRow in range(startJobRow, nBackupJobs):
            # itemRow = self.backupJobsTable.currentRow()
            item = self.backupJobsTable.item(itemRow, 0)
            backupJob = item.text()
            # print('backupJob', backupJob)

            remoteBranch = '/'.join([self.rootDir, backupSet, backupJob, self.branchPath])
            # print('remoteBranch',remoteBranch)
            if self.connectionType == 'FTP':
                dirExistsFlag = self.ftpHost.remoteDirExists(remoteBranch)
            elif self.connectionType == 'DOS':
                dirExistsFlag = os.path.exists(remoteBranch)
            #end if
            # print('dirExistsFlag',dirExistsFlag)
            if dirExistsFlag:
                if self.connectionType == 'FTP':
                    fileInfos = self.ftpHost.ftp.mlsd(remoteBranch)
                elif self.connectionType == 'DOS':
                    curDir = os.path.abspath('.')
                    os.chdir(remoteBranch)
                    fileInfos = glob.glob('*')
                    # os.chdir(curDir)
                #end if
                for fileInfo in fileInfos:
                    if self.connectionType == 'FTP':
                        # print(fileInfo[0], fileInfo[1])
                        if 'dir' in fileInfo[1]['type']:
                            continue
                        #end if
                        fileName = fileInfo[0]
                        modDate = fileInfo[1]['modify']
                        fileExt = os.path.splitext(fileName)[1]
                        fileSize = int(fileInfo[1]['size']) / 1024 # KB
                        fileSize = '%12.3f' % fileSize
                        #01234567890123
                        #20201201170713
                        #0123-45-67-89-01-23
                        #2020-12-01_17H07M13S
                        yr     = modDate[:4]
                        month  = modDate[4:6]
                        day    = modDate[6:8]
                        hour   = modDate[8:10]
                        minute = modDate[10:12]
                        sec    = modDate[12:]
                        dateString = '%s-%s-%s_%s:%s:%s' % (yr, month, day, hour,  minute, sec)
                    elif self.connectionType == 'DOS':
                        if os.path.isdir(fileInfo): continue
                        fileName = fileInfo
                        modDate = os.path.getmtime(fileName)
                        fileExt = os.path.splitext(fileName)[1]
                        fileSize = os.path.getsize(fileName)
                        fileSize = '%12.3f' % fileSize
                        yr,month,day,hour,minute,sec=time.localtime(modDate)[:-3]
                        dateString = '%s-%s-%s_%s:%s:%s' % (yr, month, day, hour,  minute, sec)
                    #end if

                    if fileName not in fileList:
                        fileList.append(fileName)
                        nRows = self.fileTable.rowCount()
                        self.fileTable.insertRow(nRows)
                        self.fileTable.setItem(nRows, 0, QTableWidgetItem(fileName))
                        self.fileTable.setItem(nRows, 1, QTableWidgetItem(fileExt))
                        self.fileTable.setItem(nRows, 3, QTableWidgetItem(fileSize))
                        # print('type(modDate)',type(modDate),modDate)
                        #          1

                        self.fileTable.setItem(nRows, 2, QTableWidgetItem(dateString))
                        self.fileTable.setItem(nRows, 4, QTableWidgetItem(backupJob))
                        print('backupJob',backupJob)
                        print('remoteBranch', remoteBranch)
                #end fileInfo
            #end if
            self.progBar.setValue(ktr)
            ktr += 1
        #end itemRow
        self.progBar.hide()
    #end populateFiles
    def getTree(self, targetDir):
        print('getTree->targetDir:',targetDir)
        treeList = []
        if self.connectionType == 'FTP':
            self.ftpHost.ftp.cwd(targetDir)
            fileInfos = self.ftpHost.ftp.mlsd()
            for fileInfo in fileInfos:
                name, infoDict = fileInfo
                if infoDict['type'] == 'dir':
                    # print(targetDir, name, infoDict['type'])
                    treeList.append(name)
                    newTarget = '%s/%s' % (targetDir, name)
                    # print('newTarget',newTarget)
                    newBranch = self.getTree(newTarget)
                    if newBranch != []:
                        treeList.append(newBranch)
                    #end if
                #end if
            #end fileInfo
        elif self.connectionType == 'DOS':
            for root, dirs, files in os.walk(targetDir):
                # print(root)
                treeList.append(root)
            # end walk
        #end if
        return treeList
    #end getTree
    def makeRemoteDir(self, remoteDir):
        # print('remoteDir', remoteDir)
        if not self.ftpHost.remoteDirExists(remoteDir):
            self.ftpHost.ftp.mkd(remoteDir)
        #end if
    #end makeRemotDir
    def makeRemoteTree(self, parentDir, folderList):
        for folder in folderList:
            if type(folder) == type('string'):
                remoteDir = '/'.join([parentDir, folder])
                self.makeRemoteDir(remoteDir)
            else:
                self.makeRemoteTree(remoteDir, folder)
            #end if
        #end folder
    #end makeRemoteTree
    def moveRemoteFile(self, srcFile, destFile, overwriteFlag=False):
        if self.ftpHost.remoteFileExists(destFile):
            if overwriteFlag:
                print('Need to delete')
            else:
                return
            #end if
        #end if
        # print('moveRemoteFile->ftp.rename',srcFile, srcFile)
        self.ftpHost.ftp.rename(srcFile, destFile)
    #end if
    def slotCompressTime(self):

        # targetDir = r'/array1/Share/WST_BackUp/WST11 C-scratch/2020-11-17_16H02M Incremental'
        # self.ftpHost.rmTree(targetDir)
        # return

        progTxtFmt = 'Processing %s: [%s] Making Tree [%s] Moving Files %i of %i [%s] Removing Tree'

        print(80*'=')
        print(' '.join('Time Compression').center(80))
        print(80*'-')
        # selectedJob = self.backupJobsTable.selectedItems()
        selectedJob = self.backupJobsTable.currentItem()
        if selectedJob is None:
            print('Nothing Selected')
            print(80 * '=')
            return
        #end if
        # print('selectedJob',selectedJob.text(), selectedJob.row())

        mergedDir = selectedJob.text()
        if 'Merged' in mergedDir:
            print('Merged Job Selected')
            print(80 * '=')
            return
        #end if
        if 'Full' in mergedDir:
            print('Full Job Selected')
            print(80 * '=')
            return
        #end if

        mergedDir = mergedDir.split(' ')[0]
        mergedDir = '%s Merged' % mergedDir

        prinTxt = 'Creating Job: %s' % mergedDir
        print(prinTxt.center(80))
        print(80*'-')

        backupSet = self.backupSetsCombo.currentText()

        # print('backupSet',backupSet)
        # print('self.rootDir', self.rootDir)

        targetRootDir = '/'.join([self.rootDir, backupSet, mergedDir])
        # print('targetRootDir', targetRootDir)
        self.makeRemoteDir(targetRootDir)

        startRow = selectedJob.row()
        nRows = self.backupJobsTable.rowCount()
        self.progBar.setMaximum(nRows-startRow)
        ktr = 0
        # self.busyLbl.show()
        # self.busyMovie.start()
        self.progBar.setValue(nRows-startRow)
        self.progBar.show()
        QApplication.processEvents()
        for iRow in range(startRow, nRows-1):
            jobItem = self.backupJobsTable.item(iRow, 0)
            jobText = jobItem.text()
            # print('jobText',jobText)
            if 'Full' in jobText:
                continue
            #end if
            if 'Merged' in jobText:
                break
            #end if
            print('Moving Job:', jobText)
            print(80*'-')

            progList = [jobText, '>', ' ', 0, 0, ' ']
            progTxt = progTxtFmt % tuple(progList)
            self.progLbl.setText(progTxt)
            self.progLbl.show()
            QApplication.processEvents()
            folderList = self.storeFolderList(jobText, backupSet)
            srcRoot = folderList[0]

            #
            #  Make the remote tree structure in the Merged folder
            #
            print(' '.join('Making Remote Tree...').center(80))
            self.makeRemoteTree(targetRootDir, folderList)

            progList = [jobText, 'X', '>', 0, 0, ' ']
            progTxt = progTxtFmt % tuple(progList)
            QApplication.processEvents()
            self.progLbl.setText(progTxt)

            print(' '.join('Moving Files...').center(80))
            srcDir = '/'.join([self.rootDir,backupSet, jobText, srcRoot])
            # print('srcDir', srcDir)
            fileList = self.getLeaves(srcDir, [])
            nFiles = len(fileList)
            fKtr = 0
            for row in fileList:
                progList = [jobText, 'X', '>', fKtr, nFiles, ' ']
                progTxt = progTxtFmt % tuple(progList)
                self.progLbl.setText(progTxt)
                QApplication.processEvents()

                srcFileFull, fileData = row
                # print(' src',srcFileFull)
                tmpTarget = '%s/%s' % (targetRootDir, srcRoot)
                destFileFull = srcFileFull.replace(srcDir, tmpTarget)
                # print('dest', destFileFull)

                self.moveRemoteFile(srcFileFull, destFileFull)

                fKtr += 1
            #end row
            progList = [jobText, 'X', 'X', nFiles, nFiles, '>']
            progTxt = progTxtFmt % tuple(progList)
            self.progLbl.setText(progTxt)
            QApplication.processEvents()

            #
            #  Delete the current Tree
            #
            # print(20*'=')
            srcDir = '/'.join([self.rootDir, backupSet, jobText])
            # print('srcDir', srcDir)
            # print('srcRoot', srcRoot)
            # print(20*'=')
            self.ftpHost.rmTree(srcDir)

            ktr += 1
            self.progBar.setValue(ktr)
            QApplication.processEvents()

            progList = [jobText, 'X', 'X', nFiles, nFiles, 'X']
            progTxt = progTxtFmt % tuple(progList)
            self.progLbl.setText(progTxt)
            QApplication.processEvents()
        #end iRow

        #
        #  Need to update folders and clean the PKL Dict and update JobTable
        #

        self.fillBackupJobTable()

        self.slotGenFolderData()

        ktr += 1
        self.progBar.setValue(ktr)
        QApplication.processEvents()

        # print(20 * '=')
        # print('hello world')
        folderFile = os.path.join(self.infoDir, myFolderFile)
        # print('folderFile', folderFile)
        f = open(folderFile, 'rb')
        folderDict = pickle.load(f)
        f.close()

        keysToRemove = []
        for key in folderDict[backupSet].keys():
            if key not in self.backupJobs:
                keysToRemove.append(key)
            #end if
        #end key
        for key in keysToRemove:
            folderDict[backupSet].pop(key)
        #end key
        f = open(folderFile, 'wb')
        pickle.dump(folderDict, f)
        f.close()

        self.progBar.hide()
        self.progLbl.hide()
        # self.busyMovie.stop()
        # self.busyLbl.hide()
        QApplication.processEvents()
        print(80*'-')
        print(' '.join('Time Compression Completed.').center(80))
        print(80*'=')

    #end slotCompressTime
    def getLeaves(self, targetDir, treeList):
        # print('getLeaves targetDir:',targetDir)
        # treeList = []
        self.ftpHost.ftp.cwd(targetDir)
        fileInfos = self.ftpHost.ftp.mlsd()
        for fileInfo in fileInfos:
            name, infoDict = fileInfo
            fullName = '%s/%s' % (targetDir, name)
            if 'dir' not in infoDict['type']:
                treeList.append([fullName, fileInfo])
            #end if
            # print('getLeaves->name', name)
            if infoDict['type'] == 'dir':
                # print(targetDir, name, infoDict['type'])
                # treeList.append(fileInfos)
                newTarget = '%s/%s' % (targetDir, name)
                # print('newTarget',newTarget)
                newBranch = self.getLeaves(newTarget, treeList)
                if newBranch != []:
                    pass
                    # treeList.append([fullName, fileInfo])
                #end if
            #end if
        #end fileInfo
        return treeList
    #end getLeaves
    def getRunInfo(self):
        if os.path.exists(self.infoFile):
            f=open(self.infoFile, 'rb')
            self.infoDict = pickle.load(f)
            f.close()
        else:
            self.infoDict = {}
        #end if
        # print(self.infoFile)
        # print(self.infoDict.keys())
        if self.localDir in self.infoDict.keys():
            # print(self.infoDict[self.localDir].keys())
            modDate = self.infoDict[self.localDir]['modDate']
            status = 'DEFINED'
        else:
            status = 'UNDEFINED'
        #end if

        if status == 'UNDEFINED':
            modDate = datetime.datetime.now()
            modDate = modDate - datetime.timedelta(weeks=4.0)
        #end if
        self.modDate = modDate

        if status == 'DEFINED':
            self.connectionType = self.infoDict[self.localDir]['connectionType']
            self.storageURL     = self.infoDict[self.localDir]['storageURL']
            self.remoteRootDir  = self.infoDict[self.localDir]['remoteRootDir']
            self.userName       = self.infoDict[self.localDir]['username']
            self.userPwd        = self.infoDict[self.localDir]['userPwd']
        else:
            self.connectionType = userDefConnectionType
            self.storageURL     = userDefstorageUrl
            self.remoteRootDir  = userDefremoteRootDir
            self.userName       = userDefuserName
            self.userPwd        = userDefuserPwd
        #end
        # self.storageURL = userDefstorageUrl
    #end getRunInfo
    def fillBackupJobTable(self):
        self.backupJobsTable.blockSignals(True)
        while self.backupJobsTable.rowCount() >0:
            self.backupJobsTable.removeRow(0)
        #end while

        self.folderTree.blockSignals(True)
        self.folderTree.clear()
        self.folderTree.blockSignals(False)

        self.fileTable.blockSignals(True)
        while self.fileTable.rowCount() > 0:
            self.fileTable.removeRow(0)
        #end while
        self.fileTable.blockSignals(False)

        backupSet = self.backupSetsCombo.currentText()

        self.backupSet = '%s/%s' % (self.rootDir, backupSet)

        if self.connectionType == 'FTP':
            self.ftpHost.ftp.cwd(self.backupSet)
            self.backupJobs = self.ftpHost.ftp.nlst()
        elif self.connectionType == 'DOS':
            os.chdir(self.backupSet)
            self.backupJobs = glob.glob('*')
        #end if
        self.backupJobs.sort()
        #
        # put the Full job at the front of the list ... table is filled in reverse order
        #
        fullJob = self.backupJobs.pop(-1)
        self.backupJobs = [fullJob] + self.backupJobs

        for backupJob in self.backupJobs:
            print(backupJob)
            self.backupJobsTable.insertRow(0)
            self.backupJobsTable.setItem(0,0, QTableWidgetItem(backupJob))
        #end backupJob
        self.backupJobsTable.blockSignals(False)
    #end fillBackupJobTable
    def slotRestore(self):
        # print(self.backupSet)
        # print(self.branchPath)
        print(80*'=')
        print(' '.join('Restoring Files ...').center(80))
        print(80*'-')
        selectedIndexes = self.fileTable.selectionModel().selectedRows()
        nIndexes = len(selectedIndexes)
        self.progBar.setMaximum(nIndexes)
        ktr = 0
        self.progBar.setValue(ktr)
        self.progBar.show()
        for selectedIndex in selectedIndexes:
            # print(selectedIndex.row())
            iRow = selectedIndex.row()
            item = self.fileTable.item(iRow, 0)
            fileName = item.text()
            item = self.fileTable.item(iRow, 4)
            backupJob = item.text()
            # print(fileName, backupJob)

            if self.connectionType == 'FTP':
                if '/' in fileName:
                    fileParts = fileName.split('/')
                    fileName = '/'.join(fileParts[1:])
                    remoteFile = '%s/%s/%s/%s' % (self.backupSet, backupJob, self.branchPath, fileName)
                else:
                    remoteFile = '%s/%s/%s/%s' % (self.backupSet, backupJob, self.branchPath, fileName)
                #end if
                fileExistsFlag = self.ftpHost.remoteFileExists(remoteFile)
            elif self.connectionType == 'DOS':
                print('slotRestoreFile-->fileName', fileName)
                remoteFile = os.path.join(self.backupSet, backupJob, self.branchPath, fileName)
                remoteFile = remoteFile.replace('/', os.path.sep)
                remoteFile = remoteFile.replace(2*os.path.sep, os.path.sep)
                fileExistsFlag = os.path.exists(remoteFile)
            #end if
            # print(remoteFile)

            # print(fileExistsFlag)
            if not fileExistsFlag:
                print('!!! Remote File Does not Exist!!!')
                print('!!! Not Restored !!!')
                continue
            #end if


            if self.connectionType == 'FTP':
                localPath = '%s/%s' % (self.branchPath, fileName)
                localPath = localPath.replace('/',os.path.sep)
                topDir = self.backupSet.split('/')[-1]
                topDir = topDir.split(' ')[-1]
                topDir = topDir.split('-')[0]
                topDir = '%s:\\' % topDir
                # print('localPath',localPath)
                localPath = os.path.join(topDir, localPath)
            elif self.connectionType == 'DOS':
                localPath = os.path.join(self.branchPath, fileName)
                topDir = self.backupSet
                topDir = topDir.split(' ')[-1]
                topDir = topDir.split('-')[0]
                topDir = '%s:\\' % topDir
                localPath = localPath.replace('/', os.path.sep)
                localPath = os.path.join(topDir, localPath)
            #end if
            # print('localPath',localPath)
            # print('topDir',topDir)
            print(' Restoring:', remoteFile)
            print('Local Path:', localPath)
            #
            #  Check for local dir
            #
            localDir = os.path.dirname(localPath)
            dirParts = localDir.split(os.path.sep)
            dirTest = dirParts[0]
            for dirPart in dirParts[1:]:
                dirTest = os.path.join(dirTest, dirPart)
                # print('dirTest', dirTest)
                if os.path.exists(dirTest):
                    if os.path.isdir(dirTest):
                        continue
                    else:
                        print('*** cannot restore ***')
                        return
                    #end if
                else:
                    os.mkdir(dirTest)
                #endif
            #end dirPart

            if os.path.exists(localPath):
                print('-->Sending old file to Trash ...')
                send2trash(localPath)
            #end if
            if self.connectionType == 'FTP':
                self.ftpHost.getFile(localPath, remoteFile)
            elif self.connectionType == 'DOS':
                shutil.copyfile(remoteFile, localPath)
            #end if
            print()
            self.progBar.setValue(ktr)
            ktr += 1
        #end selectedIndex
        self.progBar.hide()
        print(80*'-')
        print(' '.join('File Restore Completed.').center(80))
        print(80 * '=')
    #end slotRestore
#end class RestoreTool
class CompressTool():
    def __init__(self):
        # self.rootDir = '/array1/Share/WST_BackUp'
        self.rootDir = '/disk1/share/WST_Backup'
        if platform.node() == 'WST11':
            self.rootDir = '/array1/Share/WST_BackUp'
        else:
            self.rootDir = '/disk1/share/WST_Backup'
        #end if
        self.backupSets = []
        self.branchPath = ''

        self.infoDir = r'C:\scratch\WST_BackUpTool\WST_BackupInfo'
        self.infoFile = os.path.join(self.infoDir, myInfoFile)

        self.localDir = r'C:\scratch'
        self.getRunInfo()

        self.compressDays = 90

        # self.infoDict = {}

        nodeName = platform.node()

        self.ftpHost = FTPSync(self.storageURL, self.userName, self.userPwd)

        self.backupJobs = []
    #end __init__
    def getRunInfo(self):
        if os.path.exists(self.infoFile):
            f=open(self.infoFile, 'rb')
            self.infoDict = pickle.load(f)
            f.close()
        else:
            self.infoDict = {}
        #end if
        # print(self.infoFile)
        # print(self.infoDict.keys())
        if self.localDir in self.infoDict.keys():
            # print(self.infoDict[self.localDir].keys())
            modDate = self.infoDict[self.localDir]['modDate']
            status = 'DEFINED'
        else:
            status = 'UNDEFINED'
        #end if

        if status == 'UNDEFINED':
            modDate = datetime.datetime.now()
            modDate = modDate - datetime.timedelta(weeks=4.0)
        #end if
        self.modDate = modDate

        if status == 'DEFINED':
            self.connectionType = self.infoDict[self.localDir]['connectionType']
            self.storageURL     = self.infoDict[self.localDir]['storageURL']
            self.remoteRootDir  = self.infoDict[self.localDir]['remoteRootDir']
            self.userName       = self.infoDict[self.localDir]['username']
            self.userPwd        = self.infoDict[self.localDir]['userPwd']
        else:
            self.connectionType = userDefConnectionType
            self.storageURL     = userDefstorageUrl
            self.remoteRootDir  = userDefremoteRootDir
            self.userName       = userDefuserName
            self.userPwd        = userDefuserPwd
        #end
        # self.storageURL = userDefstorageUrl
    #end getRunInfo
    def makeRemoteDir(self, remoteDir):
        # print('remoteDir', remoteDir)
        if not self.ftpHost.remoteDirExists(remoteDir):
            self.ftpHost.ftp.mkd(remoteDir)
        #end if
    #end makeRemotDir
    def makeRemoteTree(self, parentDir, folderList):
        for folder in folderList:
            if type(folder) == type('string'):
                remoteDir = '/'.join([parentDir, folder])
                self.makeRemoteDir(remoteDir)
            else:
                self.makeRemoteTree(remoteDir, folder)
            #end if
        #end folder
    #end makeRemoteTree
    def storeFolderList(self, backupJob, backupSet):
        print('backupJob',backupJob)
        print('backupSet',backupSet)
        #
        #  Get list of folders
        #
        folderFile = os.path.join(self.infoDir, myFolderFile)
        # print('folderFile', folderFile)
        # print('self.backupSet', self.backupSet)
        # print('backupFolder', backupFolder)
        setKey = backupSet.split('/')[-1]
        folderKey = backupJob
        # print('setKey', setKey)
        # print('folderKey', folderKey)

        backupFolder = '%s/%s/%s' % (self.rootDir, backupSet, backupJob)

        folderFileOKFlag = False
        folderDict = {}
        if os.path.exists(folderFile):
            f=open(folderFile, 'rb')
            folderDict = pickle.load(f)
            f.close()
            if setKey in folderDict.keys():
                if folderKey in folderDict[setKey].keys():
                    folderFileOKFlag = True
                #end if
            #end if
        #end if

        if folderFileOKFlag:
            folderList = folderDict[setKey][folderKey]
        else:
            folderList = self.getTree(backupFolder)
            #
            #  Save the folderList so we don't have to do this next time
            #
            if setKey not in folderDict.keys():
                folderDict[setKey] = {}
            #end if
            folderDict[setKey][folderKey] = folderList
            f=open(folderFile, 'wb')
            pickle.dump(folderDict, f)
            f.close()
        #end if

        return folderList

    #end storeFolderList
    def getLeaves(self, targetDir, treeList):
        # print('getLeaves targetDir:',targetDir)
        # treeList = []
        self.ftpHost.ftp.cwd(targetDir)
        fileInfos = self.ftpHost.ftp.mlsd()
        for fileInfo in fileInfos:
            name, infoDict = fileInfo
            fullName = '%s/%s' % (targetDir, name)
            if 'dir' not in infoDict['type']:
                treeList.append([fullName, fileInfo])
            #end if
            # print('getLeaves->name', name)
            if infoDict['type'] == 'dir':
                # print(targetDir, name, infoDict['type'])
                # treeList.append(fileInfos)
                newTarget = '%s/%s' % (targetDir, name)
                # print('newTarget',newTarget)
                newBranch = self.getLeaves(newTarget, treeList)
                if newBranch != []:
                    pass
                    # treeList.append([fullName, fileInfo])
                #end if
            #end if
        #end fileInfo
        return treeList
    #end getLeaves
    def moveRemoteFile(self, srcFile, destFile, overwriteFlag=False):
        if self.ftpHost.remoteFileExists(destFile):
            if overwriteFlag:
                print('Need to delete')
            else:
                return
            #end if
        #end if
        # print('moveRemoteFile->ftp.rename',srcFile, srcFile)
        self.ftpHost.ftp.rename(srcFile, destFile)
    #end if
    def compressTime(self):
        # targetDir = r'/array1/Share/WST_BackUp/WST11 C-scratch/2020-11-17_16H02M Incremental'
        # self.ftpHost.rmTree(targetDir)
        # return

        progTxtFmt = 'Processing %s: [%s] Making Tree [%s] Moving Files %i of %i [%s] Removing Tree'

        print(80 * '=')
        print(' '.join('Time Compression').center(80))
        print(80 * '-')

        targetSet = 'WST11 C-scratch'
        targetDir = '%s/%s' % (self.remoteRootDir, targetSet)

        dirListing = self.ftpHost.ftp.mlsd(targetDir)
        for info in dirListing:
            print(info)
            backupJob = info[0]
            print(backupJob)
            if '.' in backupJob:
                continue
            #end if
            self.backupJobs.append(backupJob)
        #end info
        self.backupJobs.sort(reverse=True)
        today = datetime.datetime.today()
        print(today)

        ktr = 0
        for backupJob in self.backupJobs:
            # print(backupJob)
            # print(backupJob[:10])
            dateInfo = backupJob[:10]
            if '-' in dateInfo:
                [yr, mo, day] = dateInfo.split('-')
                yr  = int(yr)
                mo  = int(mo)
                day = int(day)
                # print(yr, mo, day)
                jobDate = datetime.datetime(year=yr,month=mo,day=day, minute=0,hour=0, second=0)
                delta = today-jobDate
                print(delta.days)
                if delta.days >= self.compressDays:
                    break
                #end if
            #end if
            ktr += 1
        #end backupJob

        startJobIndx = ktr

        for backupJob in self.backupJobs[startJobIndx:]:
            print(backupJob)
        #end backupJob


        # print('selectedJob',selectedJob.text(), selectedJob.row())

        mergedDir = self.backupJobs[startJobIndx]
        if 'Merged' in mergedDir:
            print('Merged Job Selected')
            print(80 * '=')
            return
        # end if
        if 'Full' in mergedDir:
            print('Full Job Selected')
            print(80 * '=')
            return
        # end if

        mergedDir = mergedDir.split(' ')[0]
        mergedDir = '%s Merged' % mergedDir

        prinTxt = 'Creating Job: %s' % mergedDir
        print(prinTxt.center(80))
        print(80 * '-')

        backupSet = targetSet



        targetRootDir = '/'.join([self.rootDir, backupSet, mergedDir])

        self.makeRemoteDir(targetRootDir)

        startRow = startJobIndx
        nRows = len(self.backupJobs)

        ktr = 0
        for iRow in range(startRow, nRows):
            jobText = self.backupJobs[iRow]
            print('jobText',jobText)
            if 'Full' in jobText:
                continue
            # end if
            if 'Merged' in jobText:
                break
            #end if
            print('Moving Job:', jobText)
            print(80 * '-')

            progList = [jobText, '>', ' ', 0, 0, ' ']
            progTxt = progTxtFmt % tuple(progList)
            print(progTxt)
            folderList = self.storeFolderList(jobText, backupSet)
            srcRoot = folderList[0]

            #
            #  Make the remote tree structure in the Merged folder
            #
            print(' '.join('Making Remote Tree...').center(80))
            self.makeRemoteTree(targetRootDir, folderList)

            progList = [jobText, 'X', '>', 0, 0, ' ']
            progTxt = progTxtFmt % tuple(progList)
            print(progTxt)

            print(' '.join('Moving Files...').center(80))
            srcDir = '/'.join([self.rootDir, backupSet, jobText, srcRoot])
            print(' '.join('Getting files to move (leaves)'))
            print('srcDir', srcDir)
            fileList = self.getLeaves(srcDir, [])
            nFiles = len(fileList)
            fKtr = 0
            for row in fileList:
                progList = [jobText, 'X', '>', fKtr, nFiles, ' ']
                progTxt = progTxtFmt % tuple(progList)
                print(progTxt)

                srcFileFull, fileData = row
                print('->srcFileFull',srcFileFull)
                tmpTarget = '%s/%s' % (targetRootDir, srcRoot)
                destFileFull = srcFileFull.replace(srcDir, tmpTarget)
                # print('dest', destFileFull)

                self.moveRemoteFile(srcFileFull, destFileFull)

                fKtr += 1
            # end row
            progList = [jobText, 'X', 'X', nFiles, nFiles, '>']
            progTxt = progTxtFmt % tuple(progList)
            print(progTxt)

            #
            #  Delete the current Tree
            #
            # print(20*'=')
            srcDir = '/'.join([self.rootDir, backupSet, jobText])
            # print('srcDir', srcDir)
            # print('srcRoot', srcRoot)
            # print(20*'=')
            self.ftpHost.rmTree(srcDir)

            ktr += 1

            progList = [jobText, 'X', 'X', nFiles, nFiles, 'X']
            progTxt = progTxtFmt % tuple(progList)
            print(progTxt)

        # end iRow
        folderFile = os.path.join(self.infoDir, myFolderFile)
        # print('folderFile', folderFile)
        f = open(folderFile, 'rb')
        folderDict = pickle.load(f)
        f.close()

        keysToRemove = []
        for key in folderDict[backupSet].keys():
            if key not in self.backupJobs:
                keysToRemove.append(key)
            # end if
        # end key
        for key in keysToRemove:
            folderDict[backupSet].pop(key)
        # end key
        f = open(folderFile, 'wb')
        pickle.dump(folderDict, f)
        f.close()

        print(80 * '-')
        print(' '.join('Time Compression Completed.').center(80))
        print(80 * '=')


    #end compressTime
#end class CompressTool