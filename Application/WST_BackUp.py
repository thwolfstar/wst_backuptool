# File:  WST_BackUp
# Author:  Tim Hunter
# Creation Date:  2020-11-18
#-----------------------------------------------
import sys
import datetime
import platform
from library.backUpTools import IncrementalBackUp
from library.backUpTools import RestoreTool
from library.backUpTools import CompressTool

from PyQt5.QtWidgets import QApplication

def compressTime():
    timeCompressor = CompressTool()
    timeCompressor.compressTime()
#end compressTime
#end compressTime
def runIncBackup(localDir):
    print(80*'=')
    incBackup = IncrementalBackUp(localDir)

    startBackupDateTime = datetime.datetime.now()
    print('Start:', startBackupDateTime)

    incBackup.runIncrementalBackup()

    endBackupDateTime = datetime.datetime.now()
    print('End:', endBackupDateTime)

    delta = endBackupDateTime - startBackupDateTime
    print('Elapsed:', delta)
    print(80 * '=')
#end runIncBackup
def restoreBackup():

    app = QApplication(sys.argv)
    restoreGUI = RestoreTool()
    restoreGUI.show()
    sys.exit(app.exec_())

#end restoreBackup
def main():
    print(80*'=')
    print(' '.join('WST Backup Running').center(80))
    print(80*'-')
    if platform.node() == 'WST11':
        localDirs = [r'D:\Reprise', r'C:\scratch', r'D:\scratch']
        # localDirs = [r'D:\Reprise', r'D:\scratch']
    elif platform.node() == 'WST12':
        localDirs = [r'D:\Reprise']
    else:
        localDirs = [r'C:\scratch']
    #end if
    for localDir in localDirs:
        print(localDir.center(80))
        print(80*'-')
        runIncBackup(localDir)
    #end localDir
#end main
if __name__ == '__main__':
    # print(sys.argv)
    # print(len(sys.argv))
    if len(sys.argv) == 1:
        main()
    elif sys.argv[1] == 'RESTORE':
        restoreBackup()
    elif sys.argv[1] == 'COMPRESS':
        compressTime()
    #end if
